<div align = center>

  <h1>HeartTrack Load Testing with K6</h1>
    
</div>

## Introduction

This repository contains load testing scripts written in K6 for HeartTrack application APIs. The purpose of these scripts is to simulate user traffic and analyze the performance of the APIs under different load conditions.

## Prerequisites

Before running the load tests, ensure you have the following installed:

- [K6](https://k6.io/docs/getting-started/installation/)

## Getting Started

To run the load tests locally, follow these steps:

1. Clone this repository:

   ```bash
   git clone https://codefirst.iut.uca.fr/git/HeartDev/K6_Project.git
   ```

2. Navigate to the project directory:

   ```bash
   cd src
   ```

3. Install dependencies:

   ```bash
   apt install k6
   ```

4. Run the load tests:

   ```bash
   k6 run script.js
   ```

## Features

- **Scripting**: Load testing scripts written in JavaScript using K6.
- **Simulation**: Simulate user traffic to API endpoints.
- **Analysis**: Analyze response times, throughput, and errors.
- **Scalability**: Scale up or down the number of virtual users to simulate real-world scenarios.

## Test Scenarios

### Scenario 1: Get User Details
- Description: Retrieve user details by ID.
- Endpoint: `/api/v1/Users/{id}`
- Method: GET
- Parameters: `id`
- Headers: `Authorization`
- Expected Response: User details JSON object.

### Scenario 2: Delete User
- Description: Delete a user by ID.
- Endpoint: `/api/v1/Users/{id}`
- Method: DELETE
- Parameters: `id`
- Headers: `Authorization`
- Expected Response: Success message or error.

### Scenario 3: Update User
- Description: Update user details.
- Endpoint: `/api/v1/Users/{id}`
- Method: PUT
- Parameters: `id`, JSON payload with updated user details
- Headers: `Authorization`, `Content-Type`
- Expected Response: Updated user details or error.

## Running Tests

To run the load tests, you can use K6's built-in CLI or integrate with your CI/CD pipeline for automated testing.

```bash
k6 run script.js
```

## Contributing

Contributions are welcome! If you have any suggestions, improvements, or new test scenarios, feel free to open an issue or create a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Acknowledgements

Special thanks to the team members of HeartTrack project for their contributions and support during load testing.