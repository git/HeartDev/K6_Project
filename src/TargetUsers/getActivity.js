import http from 'k6/http';
import { sleep, check } from 'k6';

export let options = {
  stages: [
    { duration: '1m', target: 10 }, // 10 utilisateurs pendant 1 minute
    { duration: '3m', target: 10 }, // maintenir 10 utilisateurs pendant 3 minutes
    { duration: '1m', target: 50 }, // augmentation jusqu'à 50 utilisateurs pendant 1 minute
    { duration: '3m', target: 50 }, // maintenir 50 utilisateurs pendant 3 minutes
    { duration: '1m', target: 100 }, // augmentation jusqu'à 100 utilisateurs pendant 1 minute
    { duration: '3m', target: 100 }, // maintenir 100 utilisateurs pendant 3 minutes
    { duration: '1m', target: 0 }, // diminution progressive jusqu'à 0 utilisateurs
  ],
};

export default function () {
  // Exemple de requête pour récupérer des activités
  let res = http.get('https://codefirst.iut.uca.fr/containers/HeartDev-api/api/v1/Activity?OrderingPropertyName=ByType&Descending=true&Index=0&Count=10', {
    headers: {
      'accept': 'text/plain; x-api-version=1.0',
      'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJuYW1laWQiOiI1IiwiZ2l2ZW5fbmFtZSI6InN0cmluZyIsIm5iZiI6MTcxMjUwNTM4MSwiZXhwIjoxNzEzMTEwMTgxLCJpYXQiOjE3MTI1MDUzODEsImlzcyI6IkhlYXJ0VHJhY2siLCJhdWQiOiJIZWFydFRyYWNrIn0.MOCk1K5BzO6BEd1RNUNr_z-ChO1D7e9yZA0kH3d5HgXw881jxBjW5hGw6XW-rqp0phuTjUTR87nFOjI8Pq9wsg',
    },
  });

  check(res, {
    'status is 200': (r) => r.status === 200,
    'response time is less than 500ms': (r) => r.timings.duration < 500,
  });

  sleep(1);
}