import http from 'k6/http';
import { sleep, check } from 'k6';

export const options = {
  stages: [
    { duration: '1m', target: 10 }, // 10 utilisateurs pendant 1 minute
    { duration: '3m', target: 20 }, // 20 utilisateurs pendant 3 minutes
    { duration: '1m', target: 30 }, // augmentation jusqu'à 30 utilisateurs pendant 1 minute
    { duration: '3m', target: 30 }, // maintenir 30 utilisateurs pendant 3 minutes
    { duration: '1m', target: 40 }, // augmentation jusqu'à 40 utilisateurs pendant 1 minute
    { duration: '3m', target: 40 }, // maintenir 40 utilisateurs pendant 3 minutes
    { duration: '1m', target: 50 }, // augmentation jusqu'à 50 utilisateurs pendant 1 minute
    { duration: '3m', target: 50 }, // maintenir 50 utilisateurs pendant 3 minutes
    { duration: '1m', target: 60 }, // augmentation jusqu'à 60 utilisateurs pendant 1 minute
    { duration: '3m', target: 60 }, // maintenir 60 utilisateurs pendant 3 minutes
    { duration: '1m', target: 70 }, // augmentation jusqu'à 70 utilisateurs pendant 1 minute
    { duration: '3m', target: 70 }, // maintenir 70 utilisateurs pendant 3 minutes
    { duration: '1m', target: 80 }, // augmentation jusqu'à 80 utilisateurs pendant 1 minute
    { duration: '3m', target: 80 }, // maintenir 80 utilisateurs pendant 3 minutes
    { duration: '1m', target: 90 }, // augmentation jusqu'à 90 utilisateurs pendant 1 minute
    { duration: '3m', target: 90 }, // maintenir 90 utilisateurs pendant 3 minutes
    { duration: '1m', target: 100 }, // augmentation jusqu'à 100 utilisateurs pendant 1 minute
    { duration: '3m', target: 100 }, // maintenir 100 utilisateurs pendant 3 minutes
  ],
};

export default function() {
  let res = http.get('https://codefirst.iut.uca.fr/containers/HeartDev-api/api/v1/Users?OrderingPropertyName=ByName&Descending=false&Index=0&Count=10', {
    headers: {
      'accept': 'text/plain; x-api-version=1.0',
      'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJuYW1laWQiOiI1IiwiZ2l2ZW5fbmFtZSI6InN0cmluZyIsIm5iZiI6MTcxMjUwNTM4MSwiZXhwIjoxNzEzMTEwMTgxLCJpYXQiOjE3MTI1MDUzODEsImlzcyI6IkhlYXJ0VHJhY2siLCJhdWQiOiJIZWFydFRyYWNrIn0.MOCk1K5BzO6BEd1RNUNr_z-ChO1D7e9yZA0kH3d5HgXw881jxBjW5hGw6XW-rqp0phuTjUTR87nFOjI8Pq9wsg',
    },
  });

  check(res, {
    'status is 200': (r) => r.status === 200,
  });

  sleep(1);
}
