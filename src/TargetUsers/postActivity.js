import http from 'k6/http';
import { sleep, check } from 'k6';

export const options = {
  stages: [
    { duration: '1m', target: 1 }, // 1 utilisateurs pendant 1 minute
    { duration: '3m', target: 5 }, // 5 utilisateurs pendant 3 minutes
    { duration: '1m', target: 10 }, // augmentation jusqu'à 10 utilisateurs pendant 1 minute
    { duration: '3m', target: 10 }, // maintenir 10 utilisateurs pendant 3 minutes
    { duration: '1m', target: 20 }, // augmentation jusqu'à 20 utilisateurs pendant 1 minute
    { duration: '3m', target: 20 }, // maintenir 20 utilisateurs pendant 3 minutes
    { duration: '1m', target: 30 }, // augmentation jusqu'à 30 utilisateurs pendant 1 minute
    { duration: '3m', target: 30 }, // maintenir 30 utilisateurs pendant 3 minutes
    { duration: '1m', target: 40 }, // augmentation jusqu'à 40 utilisateurs pendant 1 minute
    { duration: '3m', target: 40 }, // maintenir 40 utilisateurs pendant 3 minutes
    { duration: '1m', target: 50 }, // augmentation jusqu'à 50 utilisateurs pendant 1 minute
    { duration: '3m', target: 50 }, // maintenir 50 utilisateurs pendant 3 minutes
    { duration: '1m', target: 60 }, // augmentation jusqu'à 60 utilisateurs pendant 1 minute
    { duration: '3m', target: 60 }, // maintenir 60 utilisateurs pendant 3 minutes
    { duration: '1m', target: 70 }, // augmentation jusqu'à 70 utilisateurs pendant 1 minute
    { duration: '3m', target: 70 }, // maintenir 70 utilisateurs pendant 3 minutes
    { duration: '1m', target: 80 }, // augmentation jusqu'à 80 utilisateurs pendant 1 minute
    { duration: '3m', target: 80 }, // maintenir 80 utilisateurs pendant 3 minutes
    { duration: '1m', target: 90 }, // augmentation jusqu'à 90 utilisateurs pendant 1 minute
    { duration: '3m', target: 90 }, // maintenir 90 utilisateurs pendant 3 minutes
    { duration: '1m', target: 100 }, // augmentation jusqu'à 100 utilisateurs pendant 1 minute
    { duration: '3m', target: 100 }, // maintenir 100 utilisateurs pendant 3 minutes
  ],
};

export default function() {
  let payload = {
    activity: {
      "id": 10,
      "type": "string",
      "date": "2024-04-07T16:12:59.389Z",
      "startTime": "2024-04-07T16:12:59.389Z",
      "endTime": "2024-04-07T16:12:59.389Z",
      "effortFelt": 0,
      "variability": 0,
      "variance": 0,
      "standardDeviation": 0,
      "average": 0,
      "maximum": 0,
      "minimum": 0,
      "averageTemperature": 0,
      "hasAutoPause": true
    },
    heartRates: [
      {
        "id": 0,
        "timestamp": "2024-04-07T16:12:59.389Z",
        "latitude": 0,
        "longitude": 0,
        "altitude": 0,
        "heartRate": 0,
        "cadence": 0,
        "distance": 0,
        "speed": 0,
        "power": 0,
        "temperature": 0
      }
    ],
    athleteId: 1
  };

  let res = http.post('https://codefirst.iut.uca.fr/containers/HeartDev-api/api/v1/Activity', JSON.stringify(payload), {
    headers: {
      'accept': '*/*',
      'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJuYW1laWQiOiI1IiwiZ2l2ZW5fbmFtZSI6InN0cmluZyIsIm5iZiI6MTcxMjUwNTM4MSwiZXhwIjoxNzEzMTEwMTgxLCJpYXQiOjE3MTI1MDUzODEsImlzcyI6IkhlYXJ0VHJhY2siLCJhdWQiOiJIZWFydFRyYWNrIn0.MOCk1K5BzO6BEd1RNUNr_z-ChO1D7e9yZA0kH3d5HgXw881jxBjW5hGw6XW-rqp0phuTjUTR87nFOjI8Pq9wsg',
      'Content-Type': 'application/json; x-api-version=1.0',
    },
  });

  check(res, {
    'status is 200': (r) => r.status === 200,
  });

  sleep(1);
}
