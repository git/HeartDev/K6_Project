import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  vus: 10,
  duration: '30s',
};

export default function() {
  // Définir l'URL de l'API et les paramètres de la requête GET
  let urlGet = 'https://codefirst.iut.uca.fr/containers/HeartDev-api/api/v1/Activity?OrderingPropertyName=ByType&Descending=true&Index=0&Count=10';
  let paramsGet = {
    headers: {
      'accept': 'application/json',
      'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJuYW1laWQiOiI1IiwiZ2l2ZW5fbmFtZSI6InN0cmluZyIsIm5iZiI6MTcxMjUwNTM4MSwiZXhwIjoxNzEzMTEwMTgxLCJpYXQiOjE3MTI1MDUzODEsImlzcyI6IkhlYXJ0VHJhY2siLCJhdWQiOiJIZWFydFRyYWNrIn0.MOCk1K5BzO6BEd1RNUNr_z-ChO1D7e9yZA0kH3d5HgXw881jxBjW5hGw6XW-rqp0phuTjUTR87nFOjI8Pq9wsg'
    },
  };

  // Envoyer la requête GET
  let resGet = http.get(urlGet, paramsGet);
  
  // Attendre 1 seconde entre chaque requête
  sleep(1);

  // Définir l'URL de l'API pour la requête PUT
  let urlPut = 'https://codefirst.iut.uca.fr/containers/HeartDev-api/api/v1/Users/10';
  let payload = {
    id: 0,
    username: "String",
    lastName: "string",
    firstName: "string",
    email: "string",
    sexe: "M",
    length: 0,
    weight: 0,
    password: "string",
    dateOfBirth: "2024-04-07T16:49:08.188Z",
    profilePicture: "string",
    isCoach: true
  };
  let paramsPut = {
    headers: {
      'accept': 'text/plain; x-api-version=1.0',
      'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJuYW1laWQiOiI1IiwiZ2l2ZW5fbmFtZSI6InN0cmluZyIsIm5iZiI6MTcxMjUwNTM4MSwiZXhwIjoxNzEzMTEwMTgxLCJpYXQiOjE3MTI1MDUzODEsImlzcyI6IkhlYXJ0VHJhY2siLCJhdWQiOiJIZWFydFRyYWNrIn0.MOCk1K5BzO6BEd1RNUNr_z-ChO1D7e9yZA0kH3d5HgXw881jxBjW5hGw6XW-rqp0phuTjUTR87nFOjI8Pq9wsg',
      'Content-Type': 'application/json; x-api-version=1.0'
    },
  };

  // Envoyer la requête PUT
  let resPut = http.put(urlPut, JSON.stringify(payload), paramsPut);
}
