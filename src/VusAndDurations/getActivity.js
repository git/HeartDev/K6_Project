import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  vus: 10,
  duration: '30s',
};

export default function() {
  // Définir l'URL de l'API et les paramètres de la requête
  let url = 'https://codefirst.iut.uca.fr/containers/HeartDev-api/api/v1/Activity?OrderingPropertyName=ByType&Descending=true&Index=0&Count=10';
  let params = {
    headers: {
      'accept': 'application/json',
      'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJuYW1laWQiOiI1IiwiZ2l2ZW5fbmFtZSI6InN0cmluZyIsIm5iZiI6MTcxMjUwNTM4MSwiZXhwIjoxNzEzMTEwMTgxLCJpYXQiOjE3MTI1MDUzODEsImlzcyI6IkhlYXJ0VHJhY2siLCJhdWQiOiJIZWFydFRyYWNrIn0.MOCk1K5BzO6BEd1RNUNr_z-ChO1D7e9yZA0kH3d5HgXw881jxBjW5hGw6XW-rqp0phuTjUTR87nFOjI8Pq9wsg'
    },
  };

  // Envoyer la requête GET
  let res = http.get(url, params);
  
  // Attendre 1 seconde entre chaque requête
  sleep(1);
}
