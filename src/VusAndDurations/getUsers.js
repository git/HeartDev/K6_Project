import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  vus: 10,
  duration: '30s',
};

export default function() {
  // Définir l'URL de l'API pour la requête GET
  let urlGetUsers = 'https://codefirst.iut.uca.fr/containers/HeartDev-api/api/v1/Users?OrderingPropertyName=ByName&Descending=false&Index=0&Count=10';
  let paramsGetUsers = {
    headers: {
      'accept': 'text/plain; x-api-version=1.0',
      'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJuYW1laWQiOiI1IiwiZ2l2ZW5fbmFtZSI6InN0cmluZyIsIm5iZiI6MTcxMjUwNTM4MSwiZXhwIjoxNzEzMTEwMTgxLCJpYXQiOjE3MTI1MDUzODEsImlzcyI6IkhlYXJ0VHJhY2siLCJhdWQiOiJIZWFydFRyYWNrIn0.MOCk1K5BzO6BEd1RNUNr_z-ChO1D7e9yZA0kH3d5HgXw881jxBjW5hGw6XW-rqp0phuTjUTR87nFOjI8Pq9wsg',
    },
  };

  // Envoyer la requête GET
  let resGetUsers = http.get(urlGetUsers, paramsGetUsers);
  
  // Attendre 1 seconde entre chaque requête
  sleep(1);
}
