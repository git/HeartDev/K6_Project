import http from 'k6/http';
import { sleep, check } from 'k6';

export const options = {
  vus: 10,
  duration: '30s',
};

export default function() {
  let payload = {
    activity: {
      "id": 10,
      "type": "string",
      "date": "2024-04-07T16:12:59.389Z",
      "startTime": "2024-04-07T16:12:59.389Z",
      "endTime": "2024-04-07T16:12:59.389Z",
      "effortFelt": 0,
      "variability": 0,
      "variance": 0,
      "standardDeviation": 0,
      "average": 0,
      "maximum": 0,
      "minimum": 0,
      "averageTemperature": 0,
      "hasAutoPause": true
    },
    heartRates: [
      {
        "id": 0,
        "timestamp": "2024-04-07T16:12:59.389Z",
        "latitude": 0,
        "longitude": 0,
        "altitude": 0,
        "heartRate": 0,
        "cadence": 0,
        "distance": 0,
        "speed": 0,
        "power": 0,
        "temperature": 0
      }
    ],
    athleteId: 1
  };

  let res = http.post('https://codefirst.iut.uca.fr/containers/HeartDev-api/api/v1/Activity', JSON.stringify(payload), {
    headers: {
      'accept': '*/*',
      'Authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJuYW1laWQiOiI1IiwiZ2l2ZW5fbmFtZSI6InN0cmluZyIsIm5iZiI6MTcxMjUwNTM4MSwiZXhwIjoxNzEzMTEwMTgxLCJpYXQiOjE3MTI1MDUzODEsImlzcyI6IkhlYXJ0VHJhY2siLCJhdWQiOiJIZWFydFRyYWNrIn0.MOCk1K5BzO6BEd1RNUNr_z-ChO1D7e9yZA0kH3d5HgXw881jxBjW5hGw6XW-rqp0phuTjUTR87nFOjI8Pq9wsg',
      'Content-Type': 'application/json; x-api-version=1.0',
    },
  });

  check(res, {
    'status is 200': (r) => r.status === 200,
  });

  sleep(1);
}
